#!/usr/bin/env python
#scrivi_html.py

import webbrowser
from stato_sys import *

F = open('index.html' , 'w')

contenuto = """<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Birdie Monitoring</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="src/css/bootstrap.min.css" rel="stylesheet">
    <link href="src/css/style.css" rel="stylesheet">

  </head>
  <body>
<h1 class="text-center text-primary">Birdie Monitoring</h1>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-4">

<h2>Verifica email:</h2>
"""+connessione_web()+"""
<h2>Verifica processi attivi:</h2>
"""+processi_attivi()+"""
		</div>
		<div class="col-md-4">
		<h2>File System:</h2>		
"""+FileSystem()+"""
		</div>
		<div class="col-md-4">
                <h2>Occupazione disco:</h2>
"""+SpazioDisco()+"""
		</div>
		<div class="col-md-4">	
		<h2>Verifica servizi attivi:</h2>
"""+servizi_attivi()+"""

		</div>
	</div>
</div>

    <script src="src/js/jquery.min.js"></script>
    <script src="src/js/bootstrap.min.js"></script>
    <script src="src/js/scripts.js"></script>
  </body>
</html>

"""

F.write(contenuto)
F.close()

chrome_path = '/usr/bin/firefox'
percorso = 'file:/home/dome/lezioni/PY/index.html'
webbrowser.get(chrome_path).open(percorso)

