print( 'Inserisci il numero di righe: ' )
righe = int( input() )
print( 'Inserisci il numero di colonne: ' )
colonne = int( input() )

for riga in range( 0, righe ):
    stringa = ''
    for colonna in range( 0, colonne ):
        if colonna < ( colonne - 1 ):
            if riga < ( righe - 1 ):
                stringa += '_'
            else:
              stringa += ' '
            stringa += '|'
        else:
            if riga < ( righe - 1 ):
                stringa += '_'
            else:
              stringa += ' '
    print( stringa )
