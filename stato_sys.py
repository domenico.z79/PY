import shutil
import os
import subprocess
from email_imap_funzione import mail_nuove

def sizeof_fmt(num, suffix='B'):
    for unit in ['','K','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

#def SpazioDisco():
  #   (totale, usato, libero)=shutil.disk_usage('/')
  #  totale1=sizeof_fmt(totale)
  #  usato1=sizeof_fmt(usato)
  #  libero1=sizeof_fmt(libero)
  #  return "Dimensione disco: "+totale1+"<br>Occupato: "+usato1+"<br>Libero: "+libero1

def PeriferichePci():
    cmd=['lspci']
    lspci_output=subprocess.Popen(cmd,stdout=subprocess.PIPE);
    lspci=""
    for line in lspci_output.stdout:
        lspci=lspci+(line.decode().strip())+"<br>"
    return lspci

def FileSystem():
    cmd=['df','-h']
    filesystem_output=subprocess.Popen(cmd,stdout=subprocess.PIPE);
    fs=""
    for line in filesystem_output.stdout:
        fs=fs+(line.decode().strip())+"<br>"
    return fs

def connessione_web():
    response= os.system('ping -c 1 www.google.it')
    if response == 0:
        return mail_nuove()
    else:
        return"Non sei connesso alla rete"

def processi_attivi():
    cmd=['ps','-e', '--no-headers']
    process_output=subprocess.Popen(cmd,stdout=subprocess.PIPE);
    processi=""
    for line in process_output.stdout:
        processi=processi+(line.decode().strip())+"<br>"
    return processi
        
def servizi_attivi():
    cmd=['service','--status-all']
    service_output=subprocess.Popen(cmd,stdout=subprocess.PIPE);
    servizi=""
    for line in service_output.stdout:
        servizi=servizi+(line.decode().strip())+"<br>"
    return servizi
        





