#programma per verifica tipologia di triangolo
a=input("inserisci un intero positivo per il lato a: ")
b=input("inserisci un intero positivo per il lato b: ")
c=input("inserisci un intero positivo per il lato c: ")

#faccio un controllo unico con and che verifica i dati corretti
#is.decimal si limita a verificare che i caratteri siano decimali
dati_corretti=a.isdecimal() and b.isdecimal() and c.isdecimal()

#il risultato lo carico dentro una variabile boolena e quindi posso
#subito verificare se è true o false con not, se è true passa subito all else
#altrimenti stampa l'errore che i dati contengono un errore

if not dati_corretti:
    print("I dati inseriti contengono un errore")
    print(a,b,c)
else:
    a=int(a)
    b=int(b)
    c=int(c)
#la somma dei 2 lati deve essere sempre maggiore del terzo    
    if  a+b>c and a+c>b and b+c>a:
#uguaglianza transitiva usando and allora è equilatero
        if a==b and b==c:
            print ("Equilatero")
#elif partendo da lato a usando or (se c e una coppia di lati uguali
#posso dire che è isoscele (basta solo che 1 sia vera è isoscele
        elif a==b or a==c or b==c:
            print ("Isoscele")
#se non è i 2 sopra è Scaleno
        else:
            print ("Scaleno")
    else:
        print ("I tre interi non corrispondono a un triangolo valido")
        
        
