#!/usr/bin/env python3


def massimo (valore1, valore2, valore3):
    valore_massimo = valore1
    if valore2 > valore_massimo:
        valore_massimo = valore2
    if valore3 > valore_massimo:
        valore_massimo = valore3
    return valore_massimo

valore1=int(input())
valore2=int(input())
valore3=int(input())
    
print("Il valore massimo e' : %d tra i numeri %d, %d, %d" % (massimo(valore1,valore2,valore3),valore1,valore2,valore3))
