#!/usr/bin/env python
#scrivi_html.py

import webbrowser
from stato_sys import *
from flask import Flask


app = Flask(__name__)
@app.route("/")
def hello_world():
	
    

        contenuto = """<!DOCTYPE html>
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>Birdie Monitoring</title>

            <meta name="description" content="Source code generated using layoutit.com">
            <meta name="author" content="LayoutIt!">

            <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
            <link href="/src/css/style.css" rel="stylesheet">

          </head>
          <body>
        <h1 class="text-center text-primary">Birdie Monitoring</h1>

            <div class="container-fluid">
                <div class="row">
                        <div class="col-md-4">

        <h2>Verifica email:</h2>
        """+connessione_web()+"""
        <h2>Verifica processi attivi:</h2>
        """+processi_attivi()+"""
                        </div>
                        <div class="col-md-4">
                        <h2>Periferiche PCI:</h2>
        """+PeriferichePci()+"""
                        </div>
                        <div class="col-md-4">
                        <h2>File System:</h2>
        """+FileSystem()+"""
                        </div>
                        <div class="col-md-4">
                        <h2>Servizi attivi:</h2>

        """+servizi_attivi()+"""

                        </div>
                </div>
        </div>

            <script src="src/js/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="src/js/scripts.js"></script>
          </body>
        </html>

        """

   

        chrome_path = '/usr/bin/firefox'
        percorso = 'localhost:5000'
      #  webbrowser.get(chrome_path).open(percorso)
        return contenuto
if __name__ == "__main__":
    app.run()
